/* Dependency */ // Last Update: 2019/5/14
const { CommandoClient } = require( 'discord.js-commando' );
const Discord = require( 'discord.js' );
const SQLite = require( 'better-sqlite3' );
const config	= require( './config.json' );
const path = require( 'path' );
const rp = require( 'request-promise-native' );
const fs = require( 'fs' );

/* Instance */
const sql			= new SQLite( './role.sqlite' );
const client		= new CommandoClient({
	owner: config.ownerID,
	commandPrefix: config.prefix,
	unknownCommandResponse: false,
	//invite: 'https://discord.gg/bRCvFy9',
});

/* Global Variabe */
const mediaType = [ 'png', 'jpeg', 'jpg', 'gif', 'bmp', 'gifv' ];
client.guilds = new Map();
client.userArray = [];
eventText = config.eventText;

// get Guild object.
client.getGuild = ( guildID ) => {
	if ( !client.guilds.has( guildID ) ) client.guilds.set( guildID, { id: guildID, event: false } );
	return client.guilds.get( guildID );
}
// set Event flag.
client.setEvent = ( guildID, event ) => {
	const guild = client.getGuild( guildID );
	guild.event = event;
	client.guilds.set( guildID, guild );
	return Promise.resolve( guild );
}

/* MUSICBOT */
client.music		= require( 'discord.js-musicbot-addon' );
client.music.start( client, config.musicOptions );
// Rejcts when no key was passed or something that isn't a string is passed.
Promise.resolve( client.music.bot.changeKey( config.musicOptions.youtubeKey ) )
	.catch( ( res ) => console.error(`MUSICBOT_CHANGEKEY_ERROR: ${ res }`) );

/* PREPARE CLIENT */
client.login( config.token );
client.registry
	.registerDefaultTypes()
	.registerGroups([
		['user', 'Command for Users'],
		['owner', 'Command for Owners'],
		['voice', 'Command for VoiceStream'],
	])
	.registerDefaultGroups()
	.registerDefaultCommands({
		// set False to disable help command
		help: true,
	})
	.registerCommandsIn(path.join(__dirname, 'commands'));

client.once( 'ready' , () => {
	/* SQL Setup */
	// Create SQL Table if not exist.
	const table = sql.prepare( "SELECT count(*) FROM sqlite_master WHERE type='table' AND name = 'status';" ).get();
	if ( !table[ 'count(*)' ] ) {
		sql.prepare( "CREATE TABLE status (id TEXT PRIMARY KEY, user TEXT, guild TEXT, invite TEXT , exp INTEGER, level INTEGER, click INTEGER);" ).run();
		sql.prepare( "CREATE UNIQUE INDEX idx_status_id ON status (id);" ).run();
		sql.pragma( "journal_mode = wal" );
		sql.pragma( "synchronous = 1" );
	}
	// SQLite Function
	client.setStatus		= sql.prepare( "INSERT OR REPLACE INTO status (id, user, guild, invite, exp, level, click) VALUES (@id, @user, @guild, @invite, @exp, @level, @click)" );
	client.getStatus		= sql.prepare( "SELECT * FROM status WHERE user = ? AND guild = ? LIMIT 1" );
	client.delStatus		= sql.prepare( "DELETE FROM status WHERE id = ? LIMIT 1" );
	client.updateExp		= sql.prepare( "UPDATE status SET exp = ?, level = ? WHERE user = ? AND guild = ? LIMIT 1" );
	client.linkInvite		= sql.prepare( "SELECT user FROM status WHERE invite = ?" );
	client.leaderboard	= sql.prepare( "SELECT * FROM status WHERE guild = ? ORDER BY exp DESC LIMIT 10" );
	client.user.setActivity( "Commando" );
	/* Guild Setup */
	client.guilds.forEach ( async guild => {
		// make Directory for Each Guild. 
		//fs.mkdir( `./media/${ guild.name }`, { recursive: true }, ( err ) => { if ( err ) console.error( err ); } );
		const emojiChan = await client.getChannel( guild, 'huge-emoji' );
		// confirm emojiChannel exists, Skip if debug Mode.
		if ( emojiChan && !config.debug ) {
			// RichEmbed
			const embed = client.RichEmbed()
					.setTitle( `[ NPC ] Huge-Emoji \:robot:` )
					.addField( client.user.tag, `${ client.user }` )
			// set default avatar if User does not have one.
			if ( client.user.avatarURL ) embed.setThumbnail( client.user.avatarURL );
			else embed.setThumbnail( 'https://cdn.discordapp.com/embed/avatars/0.png' );
			// send MSG to EmojiChannel ( cache this MSG to trigger REACTION EVENT )
			Promise.resolve( emojiChan.fetchMessages() )
				.then( msgs => emojiChan.bulkDelete( msgs ) )
				.then( emojiChan.send( embed.setFooter(  "↓ 在此新增 Emoji 反應以獲得原始圖片 ↓" ) ) )
				.catch( err => console.log(`CLIENT_READY_ERR: ${ err }`) );
		}
		
		// show MSG if client is ready.
		const logChan = await client.getChannel( guild, 'log' );
		// confirm logChannel exists, Skip if debug Mode.
		if ( logChan && !config.debug ) {
			// RichEmbed
			const embed = client.RichEmbed()
				.setTitle( `[ READY ] \:ballot_box_with_check: ${ guild.name }` )
				.addField( client.user.tag, `${ client.user }` )
				.setFooter( client.user.username, client.user.avatarURL )
				.setTimestamp();
			// set default avatar if User does not have one.
			if ( client.user.avatarURL ) embed.setThumbnail( client.user.avatarURL );
			else embed.setThumbnail( 'https://cdn.discordapp.com/embed/avatars/0.png' );
			// send log...
			logChan.send( embed );
		}
	});
	return;
});

/* CLIENT ERROR EVENT */
client.on( 'error' , console.error );

/* JOIN GUILD EVENT */
//client.on( 'guildCreate', guild => {});
/* USER UPDATE EVENT */
//client.on( 'UserUpdate' , ( newUser, oldUser ) => {});

/* MEMBER JOIN EVENT */
client.on( 'guildMemberAdd' , async member => {
	// Skip if debug mode.
	if ( config.debug ) return;
	// log Member join event.
	const logChan = await client.getChannel( member.guild, 'log' );
	if ( !logChan ) return;
	// RichEmbed
	const embed = client.RichEmbed()
		.setTitle( `[ JOIN ] \:tada: ${ member.guild.name }` )
		.addField( member.user.tag, `${ member.user }` )
		.setFooter( client.user.username, client.user.avatarURL )
		.setTimestamp();
	// set default avatar if User does not have one.
	if ( member.user.avatarURL ) embed.setThumbnail( member.user.avatarURL );
	else embed.setThumbnail( 'https://cdn.discordapp.com/embed/avatars/0.png' );
	// send log...
	logChan.send( embed );
});

/* MEMBER QUIT EVENT */
client.on( 'guildMemberRemove', async member => {
	// Skip if debug mode.
	if ( config.debug ) return;
	// handle member's Role.
	const Roles = member.roles.filter( role => role.hoist === false ).array();
	const _members = await member.guild.fetchMembers();
	while( Roles.length > 0 ) {
		const Role = Roles.pop();
		// byPass @everyone & Role be used.
 		if ( Role.position === 0 || _members.some( m => m.roles.has( Role ) ) ) continue;
		Role.delete( "DELETED BY BOT" );
	}
	// delete Member Record in DB.
	let status = client.getStatus.get( member.id, member.guild.id );
	if ( status ) client.delStatus.run( `${ member.guild.id }-${ member.id }` );
	// log Member quit event.
	const logChan = await client.getChannel( member.guild, 'log' );
	if ( !logChan ) return;
	// RichEmbed
	const embed = client.RichEmbed()
		.setTitle( `[ QUIT ] \:scream: ${ member.guild.name }` )
		.addField( member.user.tag, `${ member.user }` )
		.setFooter( client.user.username, client.user.avatarURL )
		.setTimestamp();
	// set default avatar if User does not have one.
	if ( member.user.avatarURL ) embed.setThumbnail( member.user.avatarURL );
	else embed.setThumbnail( 'https://cdn.discordapp.com/embed/avatars/0.png' );
	// send log...
	logChan.send( embed );
});

/* MEMBER PRESENCE UPDATE */ // 2019/5/17
client.on( 'presenceUpdate', async ( oldMember, newMember ) => {
	if( !client.isOwner( oldMember.user ) ) return;
	/* status = idle, dnd, online, offline */
	//console.log( `presenceUpdate:\nOld: status: ${oldMember.presence.status}` );
	//console.log( `presenceUpdate:\nNew: status: ${newMember.presence.status}` );
});

/* MESSAGE REACTION REMOVE */
client.on( 'messageReactionRemove', async ( msgReact, user ) => {
	// Skip if debug mode. ( only owner message is availabe )
	if ( config.debug && !client.isOwner( user.id ) ) return;
	// Triggered if emojiName equals :pushpin:
	if ( msgReact.emoji.name === '📌' && msgReact.users.size === 0 ) msgReact.message.unpin();
});

/* MESSAGE REACTION ADD */ 
client.on( 'messageReactionAdd', async ( msgReact, user ) => {
	// Skip if debug mode. ( only owner message is availabe )
	if ( config.debug && !client.isOwner( user.id ) ) return;
	// check if Message from channel.
	if ( msgReact.message.channel.name === 'general' ) {
		if( msgReact.emoji.name === '📌' && msgReact.users.size === 1 ) msgReact.message.pin();
	}
	else if ( msgReact.message.channel.name === 'huge-emoji' && msgReact.emoji.url ) {
		return client.tempMsg( msgReact.message, client.RichEmbed().setImage( msgReact.emoji.url ), 60000 );
	}
	// Owner Function below.
	if ( !client.isOwner( user.id ) ) return;
	// test Emoji name ( unicode emoji ).
	//console.log( `${ msgReact.emoji.name }` );
	if ( msgReact.emoji.name === '🐰' ) { // case :rabbit:
		eventText.push( msgReact.message.content );
	}
	else if ( msgReact.emoji.name === '🔞' ) { // case :underage:
		const nsfwChan = await client.getChannel( msgReact.message.guild, 'rule34' );
		if ( !nsfwChan ) return;
		// make Embed Message and send to LogChannel
		let embed = client.RichEmbed().setTimestamp().setTitle( `[ MOVED ] \:incoming_envelope: #${ msgReact.message.channel.name }` ).setFooter( msgReact.message.author.username, msgReact.message.author.avatarURL );
		/* Content Exists */
		if ( msgReact.message.content ) embed = await Promise.resolve( nsfwChan.send( embed.setDescription( msgReact.message.content ) ) ).then( () => embed.setDescription('') );
		const task = [];
		/* Attachment Exists */
		const array = msgReact.message.attachments.array();
		while ( array.length > 0 ) {
			const attachment = array.pop();
			const type = /.?([^.]*)$/.exec( attachment.url ).pop();
			// send ImageURL to NSFWChannel.
			if ( mediaType.includes( type ) ) task.push( Promise.resolve( nsfwChan.send( embed.setImage( attachment.url ) ) ) );
			else task.push( Promise.resolve( nsfwChan.send( embed.setDescription( attachment.url ) ) ) );
		}
		/* URL Exists */
		const urls = await client.getURLS( msgReact.message.content );
		while( urls.length > 0 ) {
			const url = urls.pop();
			const type = /.?([^.]*)$/.exec( url ).pop();
			// send ImageURL to NSFWChannel.
			if ( mediaType.includes( type ) ) embed.setImage( url );
			else task.push( Promise.resolve( nsfwChan.send( embed.setDescription( url ) ) ) );
		}
		// delete MSG after all task is done.
		Promise.all( task ).then( () => { if ( msgReact.message ) msgReact.message.delete() });
	}
});

/* MESSAGE DELETE EVENT */
client.on( 'messageDelete', async message => {
	// check if debug mode. ( only owner message is availabe )
	if ( config.debug && !client.isOwner( message.author ) ) return;
	// Skip if Bit command or Bot message.
	if ( message.author.bot || message.content.startsWith( `${config.prefix}bit` ) ) return;
	// Skip if Silence
	if ( client.userArray.indexOf( message.author.id ) > -1 ) return;
	// check if LogChannel exists.
	const logChan = await client.getChannel( message.guild, 'log' );
	if ( !logChan ) return;
	// make Embed Message and send to LogChannel.
	let embed = client.RichEmbed()
		.setTimestamp()
		.setTitle( `[ DEL ] \:incoming_envelope: #${ message.channel.name }` )
		.setFooter( message.author.username, message.author.avatarURL );				
	/* Content Exists */
	if ( message.content ) embed = await Promise.resolve( logChan.send( embed.setDescription( message.content ) ) ).then( () => embed.setDescription('') );
	/* Attachment Exists */
	const array = message.attachments.array();
	while ( array.length >0 ) {
		const attachment = array.pop();
		const type = /.?([^.]*)$/.exec( attachment.url ).pop();
		if ( mediaType.includes( type ) ) {
			logChan.send( embed.setImage( attachment.url ) );
			// if the Origin MSG been deleted , delete in MediaChannel as well. 
			Promise.resolve( client.getChannel( message.guild, 'media' ) )
				.then( channel => channel.fetchMessages()  )
				.then( msgs => msgs.find( msg => msg.author.bot && ( msg.content.includes( attachment.url ) ) ) )
				.then( msg => { if ( msg ) msg.delete(); })
				.catch( err => console.error( `DELMSG_ATTACHMENT_ERROR: ${err}` ) );
		}
	}
	/* URL Exists */
	const urls = await client.getURLS( message.content );
	while( urls.length > 0 ) {
		const url = urls.pop();
		// if the Origin MSG been deleted , delete in MediaChannel as well. 
		Promise.resolve( client.getChannel( message.guild, 'media' ) )
			.then( channel => channel.fetchMessages() )
			.then( msgs => msgs.find( msg => msg.author.bot && ( msg.content.includes( url ) ) ) )
			.then( msg => { if ( msg ) msg.delete(); })
			.catch( err => console.error( `DELMSG_ATTACHMENT_ERROR: ${err}` ) );
	}
});

/* MESSAGE UPDATE EVENT */ // 2019/5/17
client.on( 'messageUpdate', async ( oldMsg, newMsg ) => {
	// check if debug mode. ( only owner message is availabe )
	if ( config.debug && !client.isOwner( oldMsg.author ) ) return;
	// check if Message from {general} channel.
	if ( oldMsg.channel.name !== 'general' ) return;
	// check if Msg content is valuable. // skip when Pinned. ( content stay still )
	if ( oldMsg.content.length === 0 || oldMsg.content === newMsg.content ) return;
	// check if LogChannel exists.
	const logChan = await client.getChannel( oldMsg.guild, 'log' );
	if ( !logChan ) return;
	// make Embed Message and send to LogChannel.
	const embed = client.RichEmbed()
		.setTimestamp()
		.setTitle( `[ EDIT ] \:wrench: #${ oldMsg.channel.name }` )
		.setFooter( oldMsg.author.username, oldMsg.author.avatarURL )
		.addField( "Old Message", `${ oldMsg.content }` )
		.addField( "New Message", `${ newMsg.content }` )
		//.setURL( newMsg.url );
	logChan.send( embed );
});

/* MESSAGE EVENT */
client.on( 'message', async message => {
	// check if debug mode. ( only owner message is availabe )
	if ( config.debug && !client.isOwner( message.author ) ) return;
	// by pass BOT messages. ( avoid inifity-loop )
	if ( message.author.bot ) return;

	/* GUILD EVENT */
	if ( !message.guild ) return;
	// award 1 EXP for every message.
	let status = client.getStatus.get( message.author.id, message.guild.id );
	if ( !status ) status = await Promise.resolve( client.initStat( message, message.author.id ) );
	client.gainExp( message, status, 1 );

	/* TEST AREA */
	
	/* TEST AREA */

	// check if Message from {general} channel.
	if ( message.channel.name !== 'general' ) return;
	// random Event in chance X / Y.
	if ( client.chance( 6, 1000 ) ) {
		const text = eventText[ Math.floor( Math.random() * ( eventText.length-1 ) )];
		Promise.resolve( client.emojiEvent( message, text + " \:rabbit: 哈" , 120000, 100 ) )
			.catch( err => console.log( `FUNC_EMOJIEVENT_ERROR: Currently hosting Event...` ) );
	}

	// check if MediaChannel exists.
	const mediaChan = await client.getChannel( message.guild, 'media' );
	if ( !mediaChan ) return;
	/* Attachment Exists */
	const array = message.attachments.array();
	while ( array.length > 0 ) {
		// console.log( "Attachment detected!" );
		const attachment = array.pop();
		const type = /.?([^.]*)$/.exec( attachment.url ).pop();
		// send ImageURL to MediaChannel.
		if( mediaType.includes( type ) ) mediaChan.send( attachment.url );
	}
	/* URL Exists */
	const urls = await client.getURLS( message.content );
	while( urls.length > 0 ) {
		// console.log( "URL detected!" );
		const url = urls.pop();
		// send ImageURL to MediaChannel.
		mediaChan.send( url );
	}
});

// check if member has reached Rank.
client.checkPermission = ( message, level, rank ) => {
	if ( message.author.id === config.ownerID ) return;
	if ( client.getRank( level ) < rank ) {
		client.tempMsg( message, client.music.bot.note( 'fail', `此命令只適用 [${ config.rk_table[rank] }] 以上身分組成員`) );
		return true;
	} // ENDIF
}
// check if Member is in voiceChannel.
client.checkVoiceChannel = ( message, error ) => {
	if ( !message.member.voiceChannel ) {
		// MSG: You have to join a voiceChannel to use this function.
		if ( error ) client.tempMsg( message, client.music.bot.note( 'fail', error ) );
		else client.tempMsg( message, client.music.bot.note( 'fail', "需要移動到語音頻道才能使用的命令") );
		return true;
	} // ENDIF
}
// check if voiceConnection exists in guild.
client.checkVoiceConnection = ( message, error ) => {
	if ( client.voiceConnections.find( val => val.channel.guild.id === message.guild.id ) === null ) {
		// MSG: No music is playing.
		if ( error ) client.tempMsg( message, client.music.bot.note( 'fail', error ) );
		else client.tempMsg( message, client.music.bot.note( 'fail', "音樂播放期間才能使用的命令") );
		return true;
	} // ENDIF
}
// check if musicQueue exists in guild.
client.checkQueue = ( message, error ) => {
	if ( !client.music.bot.queues.has( message.guild.id ) ) {
		// MSG: No queue for this server found!
		if ( error ) tempMsg( message, client.music.bot.note( 'fail', error ) );
		else tempMsg( message, client.music.bot.note( 'fail', "此伺服器的播放佇列尚未建立") );
		return true;
	} // ENDIF
}
// check if {max} > target > {min}.
client.checkRange = ( target, max, min ) => {
	if ( !target ) return true;
	const n = parseInt( target, 10 );
	if ( n > max || n < min ) return true;
}
// return true in chance of ( x / y ).
client.chance = ( x, y ) => {
	return Math.floor( Math.random() * y ) + 1 <= x ;
}
// send Text and delete after timeout.
client.tempMsg = ( message, text, timeout=config.timeout ) => {
	// check if timeout is available.
	if ( timeout ) {
		// send MSG and delete after timeout.
		return Promise.resolve( message.channel.send( text ) )
					.then( msg => { if ( msg ) msg.delete( timeout ) } )
					.catch( err => console.error( `FUNC_TEMPMSG_ERROR: ${ err }` ) );
	}
	return Promise.resolve( message.channel.send( text ) );
}
// create RichEmbed with random color.
client.RichEmbed = () => {
	return new Discord.RichEmbed().setColor( client.randHex() );
}
// set Role position to the top.
client.setPosition = ( Role, pos ) => {
	if ( pos < 0 ) return;
	// check if position is positive. ( default: 0 )
	Promise.resolve( Role.setPosition( pos ) )
		.catch( err => {
			console.error( `POSITION_SET_ERROR: ${err}` );
			// if Error try again with lower Position. ( pos-1 )
			client.setPosition( Role, pos-1 );
		});
}
// request File from URL and save in ./media/{folder}.
client.requestFile = ( folder, url ) => {
	// request FileStream from URL.
	Promise.resolve( client.request( { url:url, encoding: null, resolveWithFullResponse: true } ) )
		.then( res => {
			// write into the guild folder.
			fs.writeFile( `./media/${ guildname }/${ url.split('/').pop() }`, res.body, ( err ) => {
				if( err ) console.error( `WRITE_FILE_ERROR: ${err}` );
			});
		})
		.catch( err => console.error( `REQUEST_FILE_ERROR: ${err}` ) )
}
// return random Hex number.
client.randHex = () => {
	return '#' + Math.floor( Math.random()*16777215 ).toString( 16 );
}
// request URL and return with response.
client.request = ( rpOptions ) => {
	return Promise.resolve( rp( rpOptions ) );
}
// return bit.ly Shorten Options.
client.bitShortOptions = ( url ) => {
	return {
				url: "https://api-ssl.bitly.com/v4/shorten",
				method: "POST",
				json: true,
				auth: {
					sendImmediately: true,
					bearer: config.bitToken
				},
				body: {
					long_url: url,
					group_guid: config.guid
				}
			  };
}
// return bit.ly Click Options.
client.bitClickOptions = ( bitLink ) => {
	return {
					url: `https://api-ssl.bitly.com/v4/bitlinks/${ bitLink }/clicks/summary`,
					method: "GET",
					json: true,
					auth: {
						sendImmediately: true,
						bearer: config.bitToken
					}
			  };
}
// return new Status object.
client.initStat = ( message, id ) => {
	// make new Status object.
	const status =
		{
			id: `${ message.guild.id }-${ id }`,
			user: id,
			guild: message.guild.id,
			invite: undefined,
			exp: 0,
			level: 1,
			click: 0
		};
	// record in DB as well.
	client.setStatus.run( status );
	return Promise.resolve( status );
}
// get GuildChannel with channelname.
client.getChannel = ( Guild, channelname, type='text' ) => {
	return Promise.resolve( Guild.channels.find( channel => ( channel.name === channelname ) && ( channel.type === type ) ) );
}
// get URLS from Message.
client.getURLS = ( msg ) => {
	const urls = [];
	msg.replace( /https?:\/\/[^\|\s]+/g, ( input ) => urls.push( Promise.resolve( input ) ) );
	return Promise.all( urls );
}
// get Mentions UserID by parsing message content.
client.getMentions = ( message ) => {
	const mentions = [];
	message.content.replace( /<@!?[0-9]+>/g, ( input ) => mentions.push( Promise.resolve( input.replace(/<|!|>|@/g, '') ) ) );
	return Promise.all( mentions );
}
// get Invite by InviteURL.
client.getInvite = async ( message, inviteURL, chanName='general' ) => {
	if ( inviteURL ) { // check if InviteURL exists.
		const invite = await
			// check if InviteURL available.
			Promise.resolve( message.guild.fetchInvites() )
				.then( invites => invites.find( invite => invite.url === inviteURL ) )
				.catch( err => { console.error( `FUNC_GET_INVITE_ERROR: ${ err }` ) } );
		// return Invite if still available. ( if not goto default case )
		if ( invite ) return Promise.resolve( invite );
	}
	// create new InviteURL if not exists or Expired.
	const channel = await client.getChannel( message.guild, chanName );
	return Promise.resolve( channel.createInvite( { unique: true, maxAge: 0 } ) );
}
// get GuildMember with UserID.
client.getMember = ( message, userID ) => {
	return Promise.resolve( client.fetchUser( userID ) )
				.then( user => message.guild.fetchMember( user ) )
				.catch( err => console.error( `FUNC_GET_MEMBER_ERROR: ${ err }` ) );
}
// get Role with rolename. 
client.getRole = ( message, rolename, hoist=false ) => {
	return Promise.resolve( message.guild.roles.find( role => ( role.name === rolename ) && ( role.hoist === hoist ) && ( !role.hasPermission('ADMINISTRATOR') ) ) )
				.catch( err => console.error( `FUNC_GET_ROLE_ERROR: ${ err }` ) );
}
// Get Rank from rank table
client.getRank = ( level ) => {
	return config.lv_table.filter( item => item <= level ).length;
}
// calculate LV with current Exp.
client.getLevel = ( curExp ) => {
	return 1 + Math.floor( Math.sqrt( curExp * 0.1 ) );
}
// add Role to a Member.
client.addRole = async ( message, member, rolename, hoist=false ) => {
	// by pass if rolename == false.
	if ( !rolename ) return Promise.resolve( member );
	let Role = await client.getRole( message, rolename, hoist );
	// create New Role if Role not exists.
	if ( !Role ) {
		Role = await Promise.resolve( message.guild.createRole( { name: rolename, hoist:hoist, color:client.randHex() } ) )
						.catch( err => console.error( `FUNC_CREATE_ROLE_ERROR: ${ err }` ) );
	}
	// check if the Role is ADMIN.
	if ( Role.hasPermission('ADMINISTRATOR') || Role.hoist !== hoist ) {
		// @mention Rolename duplicate with default role.
		client.tempMsg( message, `${ message.author } 身分組名稱與預設身分組衝突` );
		return Promise.resolve( member );
	}
	// move the Role to the top. ( if the Role isn't default role. )
	if ( !hoist ) client.setPosition( Role, member.highestRole.position + 1 );
	// add Role to a Member.
	client.tempMsg( message, `${ member.user } 已新增身分組 [${ Role.name }]` );
	return Promise.resolve( member.addRole( Role ) )
				.catch( err => console.error( `FUNC_ADD_ROLE_ERROR: ${ err }` ) );
}
// remove Role from a Member.
client.removeRole = async ( message, member, rolename ) => {
	let Role = member.hoistRole;
	if ( rolename ) {
		Role = await member.roles.find( role => ( role.position !== 0 ) && ( role.name === rolename ) );
		if ( !Role ) return client.tempMsg( message, `${ message.author } [${ rolename }] 身分組不存在` );
	}
	if ( Role ) {
		// try to remove Role from User.
		Promise.resolve( member.removeRole( Role ) )
			.then( ()=> {
				// @mention Role has been removed [rolename].
				client.tempMsg( message, `<@${ member.user.id }> 已移除身分組 [${ Role.name }]` );
				// check if Role be used, default, @everyone
				if ( !Role.hoist ) {
					Promise.resolve( message.guild.fetchMembers() )
						.then( members => members.some( member => member.roles.has( Role ) ) )
						.then( result => { if ( !result ) Role.delete("DELETED BY BOT") } )
						.catch( err => console.error( `FUNC_DELETE_ROLE_ERROR: ${ err }` ) );
				}
			})
			.catch( err => console.error( `FUNC_REMOVE_ROLE_ERROR: ${ err }` ) );
	}
	return Promise.resolve( member );
}
// add EXP to User Status.
client.gainExp = async ( message, status, exp ) => {
	// calculate EXP and LEVEL.
	const curExp = status.exp + parseInt( exp , 10 );
	const curLevel = client.getLevel( curExp );
	status.exp = curExp;
	if ( status.level !== curLevel ) { /* case LEVEL UP */
		const preRank = client.getRank( status.level );
		const Rank = client.getRank( curLevel );
		status.level = curLevel;
		// @mention Level up to Lv.[curLevel].
		client.tempMsg( message, `<@${ status.user }> 等級提升至 Lv.${ curLevel }`);
		if ( preRank !== Rank ) { /* case RANK UP */
			const member = await client.getMember( message, status.user );
			if ( !member ) console.log( `Cannot find member.` )
			// add New Role and remove the previous.
			Promise.resolve( client.removeRole( message, member ) )
				.then( member => client.addRole( message, member, config.rk_table[Rank], true ) )
				.then( () => {
					// MSG: reach Level [lv_table] to become [rk_table].
					if ( Rank < config.lv_table.length ) client.tempMsg( message, `<@${ status.user }> 等級到達 Lv.${ config.lv_table[Rank] } 可以升級成 [${ config.rk_table[Rank+1] }]`);
				})
				.catch( err => console.error( `FUNC_GAINEXP_ERROR ${ err }` ) );
		} // ENDIF
	} // ENDIF
	client.updateExp.run( status.exp, status.level, status.user, status.guild );
	return Promise.resolve( status );
}
// host MsgEvent.
client.msgEvent = async ( message, member, time= 60000 ) => {
	if ( client.userArray.indexOf( member.id ) > -1 ) return client.tempMsg( message, `\:no_entry_sign: ${ member } 正處於沉默狀態` );
	else client.userArray.push( member.id );
	return Promise.resolve( message.channel.createMessageCollector( msg => ( msg.author.id === member.id ) && !msg.author.bot, { time: time } ) )
		.then( collector => {
			client.tempMsg( message, `${ member } \:no_entry_sign: 現在開始 ${ time/1000 } 秒內無法發送訊息` );
			/* Collector Collect EVENT */
			collector.on( 'collect', msg => {
				if ( msg.deleted ) return;
				Promise.resolve( msg.delete() )
					.then( () => client.tempMsg( message, `${ member } \:no_entry_sign: 現在無法發送訊息`, 3000 ) );
			});
			/* Collector END EVENT */
			collector.on( 'end' , async collected => {
				client.tempMsg( message, `${ member } \:speech_balloon: 現在可以發送訊息` );
				client.userArray.splice( client.userArray.indexOf( member.id ), 1 );
			});
		})
		.catch( err => console.error( `FUNC_MSGEVENT_ERROR: ${ err }` ) );
}
// host EmojiEvent.
client.emojiEvent = async ( message, text, time=60000, exp=50, del=false ) => {
	const guild = client.getGuild( message.guild.id );
	if ( guild.event ) return Promise.reject( "請等待目前的活動結束後再試。" );
	// RichEmbed
	const embed = client.RichEmbed().setTimestamp();
	if ( del ) embed.setTitle( `[ EVENT ] \:bomb: 刪除表情符號` ).setFooter( message.author.username, message.author.avatarURL );
	else embed.setTitle( `[ EVENT ] \:gift: 隨機獎勵活動` ).setFooter( client.user.username, client.user.avatarURL );
	// send MSG.
	return Promise.resolve( message.channel.send( embed.setDescription( `${ text } \n ( 對此訊息使用 Emoji 反應獲得獎勵 )` ) ) )
		.then( msg =>{
			// set Event flag. ( cannot host 2 Event at same time )
			client.setEvent( guild.id, true );
			const collector = msg.createReactionCollector( () => { return true; }, { time: time } );
			// Pin the message for quick link.
			msg.pin();
			/* Collector Collect EVENT */
			collector.on( 'collect', react => {
				if ( !react.emoji.id && del ) { /* DELETE EMOJI EVENT */
					// MSG: Cannot Delete Server Emoji !
					client.tempMsg( message, `無法刪除 ${ react.emoji } 請再試一次`, 3000 );
					react.remove( react.users.first() );
				} // ENDIF
			});
			/* Collector END EVENT */
			collector.on( 'end', async collected => {
				// MSG: Vote event has ended.
				client.tempMsg( msg, `活動結束，統計結果`, 10000 );
				const task = [];
				let winners = '';
				const reactarray = collected.array();
				while ( reactarray.length > 0 ) {
					const react = reactarray.pop();
					if ( del ) { /* DELETE EMOJI EVENT */
						// check the amount of delete request.
						if ( !react.emoji.id || react.users.size < 5 ) continue;
						// MSG: {emoji} deleted success, with votes: {votes}
						Promise.resolve( msg.channel.send(`${ react.emoji } 刪除成功, 反應次數: ${ react.users.size }`) )	
							.then( msg => msg.guild.deleteEmoji( react.emoji.id,`Vote raised By ${ message.author.tag }` ) )
							.catch( err => console.error(`EVENT_DELETE_EMOJI_ERROR: ${ err } `) );
					} // ENDIF
					// get Users in reaction.
					const userarray = react.users.array();
					while( userarray.length > 0 ) {
						const user = userarray.pop();
						// check if User already in winners list.
						if ( winners.includes( user.id ) ) continue;
						// update winners record.
						winners = winners.concat(` <@${ user.id }>`);
						let status = client.getStatus.get( user.id, message.guild.id );
						if( !status ) status = await client.initStat( message, user.id );
						// gain award for winners
						task.push( Promise.resolve( client.gainExp( message, status, exp ) ) );
					} // ENDWHILE
				} // ENDWHILE
				Promise.all( task )
					.then( () => {
						if ( winners.length > 0 ) embed.setDescription( `活動結束，參與活動獲得 ${ exp } 經驗\n${ winners }` );
						else embed.setDescription( `活動結束` );
						msg.edit( embed );
						msg.unpin();
						// set Event flag. ( cannot host 2 Event at same time )
						client.setEvent( guild.id, false );
					})
			});
		})
		.catch( err => console.error( `FUNC_EMOJIEVENT_ERROR: ${ err }` ) );
}
// Terminate the Client
client.End = () => {
	Promise.resolve( client.destroy() )
		.then( () => sql.close() )
		.catch( err => console.error( `FUNC_CLIENT_END_ERROR: ${ err }`) );
}