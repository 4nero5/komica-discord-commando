const { Command } = require( 'discord.js-commando' );

module.exports = class EmojiEventCommand extends Command {

	constructor( client ) {
		super( client, {
			name: 'emojievent',
			aliases: [],
			group: 'user',
			memberName: 'emojievent',
			description: '發起活動獲得 5 個反應的表情符號會被刪除 ( 持續 10 分鐘 )',
			guildOnly: true,
		});
	}

	async run( message ) {
		// get Status.
		let status = this.client.getStatus.get( message.author.id, message.guild.id );
		if ( !status ) status = await this.client.initStat( message, message.author.id );
		// @mention Current EXP not enough.
		if ( status.exp < 500 ) return this.client.tempMsg( message, `<@${ message.author.id }> 使用本服務還需要 ${ 500 - status.exp } 經驗` );
		Promise.resolve( this.client.emojiEvent( message, `現在開始 10 分鐘內所有超過 5 個反應的 Emoji 將被刪除`, 600000, 50, true ) )
			.then( () => {
				this.client.gainExp( message, status, -500 );
				// @mention 500 EXP has been consumed.
				this.client.tempMsg( message, `<@${ message.author.id }> 已消耗經驗值 500` );
			})
			.catch( err => this.client.tempMsg( message, `${ message.author } ${ err }` ) );
	}
};