const { Command } = require( 'discord.js-commando' );

module.exports = class StatusCommand extends Command {

	constructor( client ) {
		super( client, {
			name: 'status',
			aliases: ['stat','info'],
			group: 'user',
			memberName: 'status',
			description: '查看使用者目前等級資訊 ( 使用 ＠User#1234 指定其他使用者 )',
			guildOnly: true,
		});
	}

	async run( message ) {
		// get User ID. ( get Member ID if @mention )
		let userID = message.author.id;
		const mentions = await this.client.getMentions( message );
		if ( mentions.length > 0 ) userID = mentions.pop();
		// get Status. ( if Status not exists, create one )
		let status = this.client.getStatus.get( userID, message.guild.id );
		if ( !status ) status = await this.client.initStat( message, userID );
		// check if Invite available.
		if ( status.invite ) {
			const invite = await this.client.getInvite( message, status.invite );
			const validClick = invite.uses - status.click;
			if ( validClick > 0 ) {
				// gain 100 EXP for each valid Invite.
				status = await 
					Promise.resolve( this.client.gainExp( message, status, validClick * 100 ) )
						.then( status => {
							status.invite = invite.url;
							status.click = invite.uses;
							this.client.setStatus.run( status );
							return status;
						});
			}  // ENDIF
		} // ENDIF
		// @mention Current Level Lv.[curLevel] ( require EXP to next level [reqExp] )
		const curLevel = 1 + Math.floor( Math.sqrt( status.exp * 0.1 ) );
		const member = await this.client.getMember( message, userID );
		if ( !member ) return tempMsg( message, `${ message.author } 成員不存在` );
		let Role = member.hoistRole;
		if ( !Role ) Role = "";
		const embed = this.client.RichEmbed()
				.setTitle( `[ STATUS ] \:beginner:${ member.user.tag }` )
				.addField( `身分組`, `${ Role } ${ member.user }`, true )
				.addField( `目前等級`, `${ status.level }`, true )
				.addField( `經驗值`, `${ status.exp }` , true )
				.addField( `升級所需經驗`, `${ Math.pow( curLevel, 2 ) * 10 - status.exp }`, true )
				.setFooter( this.client.user.username, this.client.user.avatarURL )
				.setTimestamp();
		// set default avatar if User does not have one.
		if ( member.user.avatarURL ) embed.setThumbnail( member.user.avatarURL );
		else embed.setThumbnail( 'https://cdn.discordapp.com/embed/avatars/0.png' );
		this.client.tempMsg( message, embed, 120000 );
	}
};