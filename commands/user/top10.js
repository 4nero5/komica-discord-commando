const { Command } = require( 'discord.js-commando' );

module.exports = class top10Command extends Command {
	
	constructor( client ) {
		super( client, {
			name: 'top10',
			aliases: ['top'],
			group: 'user',
			memberName: 'top10',
			description: '查看經驗值排行於前 10 的使用者名單',
			guildOnly: true,
		});
	}
	
	async run( message ) {
		// get 10 user's data with EXP value in DESC order.
		const data = this.client.leaderboard.all( message.guild.id );
		// create and set Embed message.
		const embed = this.client.RichEmbed()
			.setTitle("Top 10 Members")
			.setAuthor( this.client.user.username, this.client.user.avatarURL )
			.setDescription("LeaderBoard")
		// make Embed message.
		const length = data.length;
		while ( data.length > 0 ) {
			const status = data.shift();
			const user = this.client.users.get( status.user );
			let tag = "Undefind";
			if ( user ) tag = user.tag; // tag -> nickname#number
			else this.client.delStatus.run( status.user, message.guild.id );
			embed.addField( `${ length-data.length }. LEVEL: ${ status.level }`, `${ user } ( EXP: ${ status.exp } )`);
		}
		// Send Embed message
		return this.client.tempMsg( message, { embed }, 120000 );
	}
};