const { Command } = require( 'discord.js-commando' );

module.exports = class VoidjizzCommand extends Command {
	//key is the name of the argument. When you define it in your run method, this is what you'll be using.
	//prompt is the text that displays if no argument is provided. If someone uses just ?say, that prompt will come up asking for the text.
	//type is the type the argument is a part of. This can be many things, including string, integer, user, member, etc.
	constructor( client ) {
		super( client, {
			name: 'voidjizz',
			aliases: ['void','jizz'],
			group: 'user',
			memberName: 'voidjizz',
			description: '使用技能【虛空射精】( 將消耗所有經驗值 )',
			guildOnly: true,
		});
	}
	
	async run( message ) {
		// @mention cast skill "VOIDJIZZ!" You lost all your EXP.
		this.client.tempMsg( message, `${ message.author } \u4f7f\u7528\u6280\u80fd\u3010\u865b\u7a7a\u5c04\u7cbe\u3011\uff01\u4f60\u5931\u53bb\u4e86\u6240\u6709\u7684\u7d93\u9a57\u503c`);
		// get Status. ( if Status not exists, create one )
		let status = this.client.getStatus.get( message.author.id, message.guild.id );
		if ( !status ) status = await this.client.initStat( message, message.author.id );
		// User lose all EXP.
		this.client.gainExp( message, status, -status.exp );
	}
};