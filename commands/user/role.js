﻿const { Command } = require( 'discord.js-commando' );

module.exports = class RoleCommand extends Command {	
	//key is the name of the argument. When you define it in your run method, this is what you'll be using.
	//prompt is the text that displays if no argument is provided. If someone uses just ?say, that prompt will come up asking for the text.
	//type is the type the argument is a part of. This can be many things, including string, integer, user, member, etc.
	constructor( client ) {
		super( client, {
			name: 'role',
			aliases: [],
			group: 'user',
			memberName: 'role',
			description: '新增/加入 自訂的身分組 ( 使用 ＠User#1234 指定其他用戶 )',
			guildOnly: true,
			args: [
				{
					key: 'rolename',
					prompt: '請輸入要新增的身分組名稱',
					type: 'string',
					parse: val => {
						if ( val.indexOf(' ') > 0 ) return val.slice( 0, val.indexOf(' ') );
						else return val;
					},
				},
			],
		});
	}
	
	// this.client.guilds.size;
	async run( message, { rolename } ) {
		// get Status.
		let status = this.client.getStatus.get( message.author.id, message.guild.id );
		if ( !status ) status = await this.client.initStat( message, message.author.id );
		// @mention Current EXP not enough.
		if ( status.exp < 250 ) return this.client.tempMsg( message, `${ message.author } 使用本服務還需要 ${ 250 - status.exp } 經驗` );
		// get Member if mentions.
		const mention = await Promise.resolve( this.client.getMentions( message ) ).then( mentions => mentions.pop() );
		let member = await this.client.getMember( message, message.author.id );
		if ( mention ) member = await this.client.getMember( message, mention );
		const Role = await this.client.getRole( message, rolename );
		if ( Role ) {
			if ( member.roles.has( Role.id ) ) {
				return Promise.resolve( Role.setColor( this.client.randHex() ) )
							.then( () => {
								this.client.gainExp( message, status, -250 );
								// @mention 250 EXP has been consume.
								this.client.tempMsg( message, `${ message.author } 已消耗經驗值 250` );
								// @mention Request role color has been changed.
								this.client.tempMsg( message, `${ member.user } [${ rolename }] 身分組顏色已更新` );
							})
							.catch( err => console.error(`CMD_ROLE_COLOR_ERROR: ${ err }`) );
			}
		}
		// try to add New Role to the Member.
		Promise.resolve( this.client.addRole( message, member, rolename ) )
			.then( () => {
				this.client.gainExp( message, status, -250 );
				// @mention 250 EXP has been consume.
				this.client.tempMsg( message, `${ message.author } 已消耗經驗值 250` );
			})
			.catch( err => console.error(`CMD_ROLE_ADD_ERROR: ${ err }`) );
	}
}