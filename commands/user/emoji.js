const { Command } = require( 'discord.js-commando' );

module.exports = class EmojiCommand extends Command {

	constructor( client ) {
		super( client, {
			name: 'emoji',
			aliases: [],
			group: 'user',
			memberName: 'emoji',
			description: '上傳圖片並新增表情符號 ( 請將指令填入檔案描述 )',
			guildOnly: true,
			args: [
				{
					key: 'emojiname',
					prompt: 'What text would you like the bot to say?',
					type: 'string',
					default: 'NewEmoji',
				},
			],
		});
	}
	
	// this.client.guilds.size;
	async run( message, { emojiname } ) {
		// get User Status.
		let status = this.client.getStatus.get( message.author.id, message.guild.id );
		if ( !status ) status = await this.client.initStat( message, message.author.id );
		// @mention Current EXP not enough.
		if ( status.exp < 250 ) return this.client.tempMsg( message, `${ message.author } 使用本服務還需要 ${ 250 - status.exp } 經驗` );
		// check if Attachments available.
		if ( message.attachments.size > 0 ) {
			const imageUrl = message.attachments.first().url;
			// create Emoji and consume 250 EXP.
			Promise.resolve( message.guild.createEmoji( imageUrl, emojiname ) )
				.then( emoji => {
					this.client.gainExp( message, status, -250 );
					// @mention 250 EXP has been consumed.
					this.client.tempMsg( message, `${ message.author } 已消耗經驗值 250` );
					// @mention ${ Emoji } has been added.
					this.client.tempMsg( message, `${ message.author } ${ emoji } 已新增`);
				})
				.catch( err => {
					// @mention Image size is limited below 256 KB. // @mention Maximum Server Emoji Limit
					if ( err.toString().includes( '256' ) ) this.client.tempMsg( message, `${ message.author }>圖片大小請勿超過 256 KB `);
					// @mention Emoji recahed maximum limit. ( Default: 50 )
					else if ( err.toString().includes( 'Maximum' ) ) this.client.tempMsg( message, `${ message.author } Emoji 數量超過上限 ( Maximum:50 )` );
					else console.error(`CMD_EMOJI_CREATE_ERROR: ${ err }`);
				});
		} // @mention Image does not exists.
		else return this.client.tempMsg( message, ` ${ message.author } 圖片不存在! ` )
	}
};