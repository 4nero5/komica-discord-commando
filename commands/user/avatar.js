const { Command } = require( 'discord.js-commando' );

module.exports = class AvatarCommand extends Command {

	constructor( client ) {
		super( client, {
			name: 'avatar',
			aliases: [],
			group: 'user',
			memberName: 'avatar',
			description: '顯示使用者的頭像圖片 ( 使用 ＠User#1234 指定其他使用者 )',
			guildOnly: true,
		});
	}
	
	async run( message ) {
		// get User ID. ( get Member ID if @mention )
		let user = message.author;
		const mention = await Promise.resolve( this.client.getMentions( message ) ).then( mentions => mentions.pop() );
		if ( mention ) user = await this.client.fetchUser( mention );
		// create RichEmbed and send.
		const embed = this.client.RichEmbed()
			.setImage( user.avatarURL )
			.setAuthor( user.tag, user.avatarURL );
		return this.client.tempMsg( message, embed, 60000 );
	}
};