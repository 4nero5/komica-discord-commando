const { Command } = require( 'discord.js-commando' );

module.exports = class InviteCommand extends Command {
	//key is the name of the argument. When you define it in your run method, this is what you'll be using.
	//prompt is the text that displays if no argument is provided. If someone uses just ?say, that prompt will come up asking for the text.
	//type is the type the argument is a part of. This can be many things, including string, integer, user, member, etc.
	constructor( client ) {
		super( client, {
			name: 'invite',
			aliases: [],
			group: 'user',
			memberName: 'invite',
			description: '查看使用者專屬的邀請連結 ( 每次成功邀請可獲得 100 經驗 )',
			guildOnly: true,
		});
	}
	async run( message ) {
		// get Status.
		let status = this.client.getStatus.get( message.author.id, message.guild.id );
		if ( !status ) status = await this.client.initStat( message, message.author.id );
		// get last Mention.
		if ( this.client.isOwner( message.author ) ) {
			const mention = await Promise.resolve( this.client.getMentions( message ) ).then( mentions => mentions.pop() );
			if ( mention ) {
				status = await Promise.resolve( this.client.getStatus.get( mention, message.guild.id ) );
				if ( !status ) status = await Promise.resolve( this.client.initStat( message, mention ) );
			}
		}
		// try to get Invite from Status.
		const invite = await this.client.getInvite( message, status.invite );
		if ( invite ){
			this.client.tempMsg( message, `${ message.author } 邀請碼: <${ invite.url }>`, 60000 );
			// update Invite Link in Status.
			//console.dir( status );
			status.invite = invite.url;
			status.click = invite.uses;
			this.client.setStatus.run( status );
		}
	}
};