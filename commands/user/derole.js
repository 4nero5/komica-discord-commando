const { Command } = require( 'discord.js-commando' );

module.exports = class deRoleCommand extends Command {

	constructor( client ) {
		super( client, {
			name: 'derole',
			aliases: [],
			group: 'user',
			memberName: 'derole',
			description: '移除使用者所指定的身分組 ( 只對自訂身分組有效 )',
			guildOnly: true,
			args: [
				{
					key: 'rolename',
					prompt: '請輸入要移除的身分組名稱',
					type: 'string',
					parse: val => {
						if ( val.indexOf(' ') > 0 ) return val.slice( 0, val.indexOf(' ') );
						else return val;
					},
				},
			],
		});
	}
	
	async run( message, { rolename } ) {
		// get Member if mention. ( only available for Owner )
		let member = await this.client.getMember( message, message.author.id );
		if ( this.client.isOwner ) {
			const mention = await Promise.resolve( this.client.getMentions( message ) ).then( mentions => mentions.pop() );
			if ( mention ) member = await this.client.getMember( message, mention );
		}
		// try to remove the Role from Member.
		Promise.resolve( this.client.removeRole( message, member, rolename ) )
			.catch( err => console.error(`CMD_ROLE_REMOVE_ERROR: ${ err }`) );
	}
};