const { Command } = require( 'discord.js-commando' );

module.exports = class SilenceCommand extends Command {

	constructor( client ) {
		super( client, {
			name: 'silence',
			aliases: [],
			group: 'user',
			memberName: 'silence',
			description: '所指定的使用者禁止發言 ( 持續 1 分鐘 )',
			guildOnly: true,
			args: [
				{
					key: 'member',
					prompt: '使用 @User#1234 來指定使用者',
					type: 'member',
				},
			],
		});
	}

	async run( message, { member } ) {
		if ( this.client.userArray.indexOf( message.author.id ) > -1 ) return this.client.tempMsg( message, `\:rage: 噓!` , 3000 );
		if ( this.client.userArray.indexOf( member.id ) > -1 ) return this.client.tempMsg( message, `\:no_entry_sign: ${ member } 正處於沉默狀態` );
		// get Status.
		let status = this.client.getStatus.get( message.author.id, message.guild.id );
		if ( !status ) status = await this.client.initStat( message, message.author.id );
		// @mention Current EXP not enough.
		if ( status.exp < 500 ) return this.client.tempMsg( message, `<@${ message.author.id }> 使用本服務還需要 ${ 500 - status.exp } 經驗` );
		Promise.resolve( this.client.msgEvent( message, member, 300000 ) )
			.then( () => {
				this.client.gainExp( message, status, -500 );
				// @mention 500 EXP has been consumed.
				this.client.tempMsg( message, `<@${ message.author.id }> 已消耗經驗值 500` );
			})
			.catch( err => console.error( `CMD_EMOJIEVENT_ERROR: ${ err }` ) );
	}
};