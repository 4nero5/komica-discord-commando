const { Command } = require( 'discord.js-commando' );

module.exports = class ResumeCommand extends Command {
	//key is the name of the argument. When you define it in your run method, this is what you'll be using.
	//prompt is the text that displays if no argument is provided. If someone uses just ?say, that prompt will come up asking for the text.
	//type is the type the argument is a part of. This can be many things, including string, integer, user, member, etc.
	constructor( client ) {
		super( client, {
			name: 'resume',
			aliases: [],
			group: 'voice',
			memberName: 'resume',
			description: '查看個人目前等級資訊 ( 使用 ＠User#0000 指定其他用戶 )',
			guildOnly: true,
		});
	}
	
	run( message ) {
		if ( this.client.checkVoiceConnection( message ) ) return;
		return this.client.music.bot.resumeFunction( message );
	}
};