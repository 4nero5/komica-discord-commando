const { Command } = require( 'discord.js-commando' );

module.exports = class SearchCommand extends Command {
	//key is the name of the argument. When you define it in your run method, this is what you'll be using.
	//prompt is the text that displays if no argument is provided. If someone uses just ?say, that prompt will come up asking for the text.
	//type is the type the argument is a part of. This can be many things, including string, integer, user, member, etc.
	constructor( client ) {
		super( client, {
			name: 'search',
			aliases: [],
			group: 'voice',
			memberName: 'search',
			description: '查看個人目前等級資訊 ( 使用 ＠User#0000 指定其他用戶 )',
			guildOnly: true,
			args: [
				{
					key: 'keyword',
					prompt: 'What text would you like the bot to say?',
					type: 'string',
				},
			],
		});
	}
	
	run( message, { keyword } ) {
		let status = this.client.getStatus.get( message.author.id, message.guild.id );
		if ( this.client.checkPermission( message, status.level, 3 ) ) return;
		if ( this.client.checkVoiceChannel( message ) ) return;
		if ( !keyword ) return this.client.tempMsg( message, client.music.bot.note( 'fail', "Usage: +SEARCH [Keyword]") );
		return this.client.music.bot.searchFunction( message, keyword );
	}
};