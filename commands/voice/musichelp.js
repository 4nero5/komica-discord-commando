const { Command } = require( 'discord.js-commando' );

module.exports = class MusicHelpCommand extends Command {
	//key is the name of the argument. When you define it in your run method, this is what you'll be using.
	//prompt is the text that displays if no argument is provided. If someone uses just ?say, that prompt will come up asking for the text.
	//type is the type the argument is a part of. This can be many things, including string, integer, user, member, etc.
	constructor( client ) {
		super( client, {
			name: 'musichelp',
			aliases: [],
			group: 'voice',
			memberName: 'musichelp',
			description: '查看個人目前等級資訊 ( 使用 ＠User#0000 指定其他用戶 )',
			guildOnly: true,
			args: [
				{
					key: 'musicCommand',
					prompt: 'What text would you like the bot to say?',
					type: 'string',
					default: '',
				},
			],
		});
	}
	
	run( message, { musicCommand } ) {
		if ( this.client.music.bot.commandsArray.find( command => command.name === musicCommand ) ) return this.client.music.bot.helpFunction( message, musicCommand );
		else{
			const embed = this.client.RichEmbed()
				.setTitle("Commands:")
				.setAuthor( this.client.user.username, this.client.user.avatarURL )
				.setDescription(`輸入 ${ this.client.commandPrefix }help [Command] 獲得更多訊息`);
			for ( let i = 0; i < this.client.music.bot.commandsArray.length; i++ ) {
				if ( !this.client.music.bot.commandsArray[i].exclude ) embed.addField( `${ this.client.commandPrefix }${ this.client.music.bot.commandsArray[i].name }`, this.client.music.bot.commandsArray[i].help );
			}
			return this.client.tempMsg( message, { embed }, 120000 );
		}
	}
};