const { Command } = require( 'discord.js-commando' );

module.exports = class StreamCommand extends Command {
	//key is the name of the argument. When you define it in your run method, this is what you'll be using.
	//prompt is the text that displays if no argument is provided. If someone uses just ?say, that prompt will come up asking for the text.
	//type is the type the argument is a part of. This can be many things, including string, integer, user, member, etc.
	constructor( client ) {
		super( client, {
			name: 'stream',
			aliases: [],
			group: 'voice',
			memberName: 'stream',
			description: '查看個人目前等級資訊 ( 使用 ＠User#0000 指定其他用戶 )',
			guildOnly: true,
		});
	}
	
	run( message ) {
		if ( this.client.checkVoiceChannel( message, `You have to be in the VoiceChannel to use this command` ) ) return;
		return message.say(`${ message.author } https://discordapp.com/channels/${message.guild.id}/${ message.member.voiceChannel.id } ${ message.member.voiceChannel }`);
	}
};