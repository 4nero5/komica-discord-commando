const { Command } = require( 'discord.js-commando' );

module.exports = class LeaveCommand extends Command {
	//key is the name of the argument. When you define it in your run method, this is what you'll be using.
	//prompt is the text that displays if no argument is provided. If someone uses just ?say, that prompt will come up asking for the text.
	//type is the type the argument is a part of. This can be many things, including string, integer, user, member, etc.
	constructor( client ) {
		super( client, {
			name: 'leave',
			aliases: [],
			group: 'voice',
			memberName: 'leave',
			description: '查看個人目前等級資訊 ( 使用 ＠User#0000 指定其他用戶 )',
			guildOnly: true,
		});
	}
	
	run( message ) {
		let status = this.client.getStatus.get( message.author.id, message.guild.id );
		if ( this.client.checkPermission( message, status.level, 3 ) ) return;
		// MSG: I have to be in the voiceChannel.
		if ( this.client.checkVoiceConnection( message, "\u6b64\u547d\u4ee4\u53ea\u6709\u5728\u64ad\u653e\u97f3\u6a02\u671f\u9593\u6709\u6548" ) ) return; /* I am not in a voice channel */
		return this.client.music.bot.leaveFunction( message );
	}
};