const { Command } = require( 'discord.js-commando' );

module.exports = class LinkCommand extends Command {
	//key is the name of the argument. When you define it in your run method, this is what you'll be using.
	//prompt is the text that displays if no argument is provided. If someone uses just ?say, that prompt will come up asking for the text.
	//type is the type the argument is a part of. This can be many things, including string, integer, user, member, etc.
	constructor( client ) {
		super( client, {
			name: 'link',
			aliases: [],
			group: 'owner',
			memberName: 'link',
			description: '查看個人目前等級資訊 ( 使用 ＠User#0000 指定其他用戶 )',
			ownerOnly: true,
			args: [
				{
					key: 'inviteCode',
					prompt: 'What text would you like the bot to say?',
					type: 'string',
				},
			],
		});
	}
	async run( message, { inviteCode } ) {
			const resultSet = this.client.linkInvite.get( `https://discord.gg/${ inviteCode }` );
			if( !resultSet ) return this.client.tempMsg( message, `${ message.author } 使用者不存在`);
			const user = this.client.users.get( resultSet.user );
			// @mention is the owner of { InviteURL }.
			message.author.send( `https://discord.gg/${ inviteCode } 是【${ user.tag }】的邀請碼` );
	}
};