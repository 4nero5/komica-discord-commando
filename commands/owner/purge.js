const { Command } = require( 'discord.js-commando' );

module.exports = class PurgeCommand extends Command {
	//key is the name of the argument. When you define it in your run method, this is what you'll be using.
	//prompt is the text that displays if no argument is provided. If someone uses just ?say, that prompt will come up asking for the text.
	//type is the type the argument is a part of. This can be many things, including string, integer, user, member, etc.
	constructor( client ) {
		super( client, {
			name: 'purge',
			aliases: [],
			group: 'owner',
			memberName: 'purge',
			description: '查看個人目前等級資訊 ( 使用 ＠User#0000 指定其他用戶 )',
			guildOnly: true,
			ownerOnly: true,
			args: [
				{
					key: 'deleteCount',
					prompt: 'What text would you like the bot to say?',
					type: 'integer',
					default: 11,
					validate: deleteCount => deleteCount >= 2 && deleteCount <= 100
				},
			],
		});
	}
	async run( message, {deleteCount} ) {
		// This command removes all messages from all users in the channel, up to 100.
		Promise.resolve( message.channel.fetchMessages( { limit: deleteCount } ) )
			.then( msgs => message.channel.bulkDelete( msgs ) )
			.catch( err => console.error( 'CMD_PURGE_ERROR: ${ err }' ) );
	}
};