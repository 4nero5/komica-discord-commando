const { Command } = require( 'discord.js-commando' );

module.exports = class GuildJoinCommand extends Command {
	//key is the name of the argument. When you define it in your run method, this is what you'll be using.
	//prompt is the text that displays if no argument is provided. If someone uses just ?say, that prompt will come up asking for the text.
	//type is the type the argument is a part of. This can be many things, including string, integer, user, member, etc.
	constructor( client ) {
		super( client, {
			name: 'guildjoin',
			aliases: [],
			group: 'owner',
			memberName: 'guildjoin',
			description: '查看個人目前等級資訊 ( 使用 ＠User#0000 指定其他用戶 )',
			guildOnly: true,
			ownerOnly: true,
		});
	}
	run( message ) {
			// generate and send Oauth2 invite for Guild by PM.
			Promise.resolve( this.client.generateInvite(['ADMINISTRATOR']) )
				.then( link => message.author.send(`<@${ message.author.id }> Oauth2 \u9a57\u8b49\u7db2\u5740: ${ link }`) )
				//.then( msg => message.delete().catch(O_o=>{}) )
				.catch( err => console.error(`CMD_GUILD_JOIN_ERROR: ${ err }`) );
	}
};