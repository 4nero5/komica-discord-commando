const { Command } = require( 'discord.js-commando' );

module.exports = class EndCommand extends Command {
	//key is the name of the argument. When you define it in your run method, this is what you'll be using.
	//prompt is the text that displays if no argument is provided. If someone uses just ?say, that prompt will come up asking for the text.
	//type is the type the argument is a part of. This can be many things, including string, integer, user, member, etc.
	constructor( client ) {
		super( client, {
			name: 'end',
			aliases: [],
			group: 'owner',
			memberName: 'end',
			description: '查看個人目前等級資訊 ( 使用 ＠User#0000 指定其他用戶 )',
			guildOnly: true,
			ownerOnly: true,
		});
	}
	run( message ) {
			const voiceConnection = this.client.voiceConnections.find( val => val.channel.guild.id === message.guild.id );
			if ( voiceConnection != null ) {
				const filter = m => ( m.content.includes( "Playback finished" ) > 0 ) && m.author.bot;
				this.client.music.bot.leaveFunction( message, args[0] );
				// terminate Client after leaveFunction finished message.
				Promise.resolve( message.channel.awaitMessages( filter, { max:1, time:60000, errors: ['time'] } ) )
					.then( collected => message.channel.send("\:vulcan: Salut!") )
					.then( setTimeout( () => this.client.End(), 3000 ) )
					.catch( err => console.error(`CMD_CLIENT_END_ERROR: ${ err }`) );
			}
			else {
				// terminate Client instance.
				Promise.resolve( message.channel.send("\:vulcan: Salut!") )
					.then( () => this.client.End() )
					.catch( err => console.error(`CMD_CLIENT_END_ERROR: ${ err }`) );
			}
	}
};