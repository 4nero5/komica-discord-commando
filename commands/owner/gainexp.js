const { Command } = require( 'discord.js-commando' );

module.exports = class GainExpCommand extends Command {
	//name is the name of the command.
	//aliases are other ways the command can be called. You can have as many as you want!
	//group is the command group the command is a part of.
	//memberName is the name of the command within the group (this can be different from the name).
	//description is the help text displayed when the help command is used.
	
	//key is the name of the argument. When you define it in your run method, this is what you'll be using.
	//prompt is the text that displays if no argument is provided. If someone uses just ?say, that prompt will come up asking for the text.
	//type is the type the argument is a part of. This can be many things, including string, integer, user, member, etc.
	constructor( client ) {
		super( client, {
			name: 'gainexp',
			aliases: ['gain','exp'],
			group: 'owner',
			memberName: 'gainexp',
			description: '查看個人目前等級資訊 ( 使用 ＠User#0000 指定其他用戶 )',
			guildOnly: true,
			ownerOnly: true,
			args: [
				{
					key: 'exp',
					prompt: 'What text would you like the bot to say?',
					type: 'integer',
					default: 1000,
				},
			],
		});
	}
	async run( message, { exp } ) {
		let userID = message.author.id;
		// get Mentions in message.
		const mentions = await this.client.getMentions( message );
		if ( mentions.length > 0 ) userID = mentions.pop();
		// get Status with mention ( userID ).
		let status = this.client.getStatus.get( userID, message.guild.id );
		if ( !status ) status = await Promise.resolve( this.client.initStat( message, userID ) );
		// User gain EXP.
		this.client.gainExp( message, status, exp );
	}
};