const { Command } = require( 'discord.js-commando' );

module.exports = class BitCommand extends Command {
	//key is the name of the argument. When you define it in your run method, this is what you'll be using.
	//prompt is the text that displays if no argument is provided. If someone uses just ?say, that prompt will come up asking for the text.
	//type is the type the argument is a part of. This can be many things, including string, integer, user, member, etc.
	constructor( client ) {
		super( client, {
			name: 'bit',
			aliases: [],
			group: 'owner',
			memberName: 'bit',
			description: '查看個人目前等級資訊 ( 使用 ＠User#0000 指定其他用戶 )',
			guildOnly: true,
			ownerOnly: true,
		});
	}
	async run( message ) {
			// get Message without command.
			let msg2send = message.argString;
			// get URLS from message.
			const urls = await this.client.getURLS( msg2send );
			while( urls.length > 0 ) {
				const url = urls.pop();
				msg2send = await // BLOCK
					// create Bit Link for each URL.
					Promise.resolve( this.client.request( this.client.bitShortOptions( url ) ) )
						.then( body => message.argString.replace( url, body.link ) )
						.catch( err => console.error( `CMD_BIT_LINK_ERROR: ${ err }` ) );
			}
			// send Text and delete original Message
			Promise.resolve( message.channel.send( msg2send ) )
				.then( () => message.delete().catch(O_o=>{}) )
				.catch( err => console.error( `CMD_BIT_SEND_ERROR: ${ err }` ) );
	}
};